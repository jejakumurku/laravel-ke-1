@extends('layout.master')
@section('judul')
    Halaman Form
@endsection
@section('content')
		<h1>Buat Account Baru!</h1>
		<h2>Sign Up Form</h2>
        <form action ="/welcome" method="POST">
            @csrf
		<p>First Name</p>
			<input type="text" name="firstname" />
		<p>Last Name</p>
			<input type="text" name="lastname" />
		<p>
            <label>Gender :</label> </br>
            <label><input type="radio" name="gender" value="Male" /> Male</label></br>
            <label><input type="radio" name="gender" value="female" /> Female</label></br>
            <label><input type="radio" name="gender" value="Other" /> Other</label>
        </p>
        <p>
        	<label for ="nationality">Nationality :</label> </br>
            	<select name="nationality" id="nationality">
					  <option value="Indonesian">Indonesian</option>
					  <option value="Malaysian">Malaysian</option>
					  <option value="Singapore">Singapore</option>
					  <option value="United States">United States</option>
				</select>
        </p>


       <p>
       	<label>Langue Spoken :</label> </br>
	       	<input type="checkbox" id="indonesia" name="indonesia" value="indonesia">
				<label for="indonesia"> Bahasa Indonesia</label><br>
				<input type="checkbox" id="english" name="english" value="english">
				<label for="english"> English</label><br>
				<input type="checkbox" id="other" name="other" value="other">
				<label for="other"> Other</label><br>
       </p>
       <p>
       	<label>Bio :</label> </br>
       		<textarea id="w3review" name="w3review" rows="4" cols="50"></textarea>
       </p>
       <input type="submit" value="kirim">
@endsection