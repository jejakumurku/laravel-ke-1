<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register()
    {
        return view('register');
    }
    public function welcome(Request $request)
    {
        $namaawal = $request -> firstname;
        $namaakhir = $request -> lastname;
        return view('welcome', compact('namaawal', 'namaakhir'));
    }
}
